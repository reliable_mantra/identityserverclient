﻿function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerHTML += msg + '\r\n';
    });
}

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("api").addEventListener("click", api, false);
document.getElementById("logout").addEventListener("click", logout, false);

var config = {
    authority: "https://identityserver-id.azurewebsites.net",
    client_id: "js",
    redirect_uri: "https://identityserver-spa.azurewebsites.net/callback.html",
    response_type: "id_token token",
    scope: "openid profile api",
    post_logout_redirect_uri: "https://identityserver-spa.azurewebsites.net/index.html",
    automaticSilentRenew: true,
    silent_redirect_uri: "https://identityserver-spa.azurewebsites.net/silent-renew.html"
};

var mgr = new Oidc.UserManager(config);
mgr.events.addSilentRenewError(function (error) {
    console.error("Silent Renew Failed " + error)
    mgr.signinRedirect();
});

mgr.getUser().then(function (user) {
    if (user) {
        log("User logged in", user.profile);
    }
    else {
        log("User not logged in");
    }
});

function login() {
    mgr.signinRedirect();
}

function api() {
    mgr.getUser().then(function (user) {
        console.trace("Authorization Bearer " + user.access_token);
    });
}

function logout() {
    mgr.signoutRedirect();
}